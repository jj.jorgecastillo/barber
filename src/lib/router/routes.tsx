import type { PathRouteProps } from "react-router-dom";

import Admin from "lib/pages/admin";
import Home from "lib/pages/home";
import LadiesHaircut from "lib/pages/LadiesHaircut";
import MensHaircut from "lib/pages/MensHaircut";

export const routes: Array<PathRouteProps> = [
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/ladies-haircut",
    element: <LadiesHaircut />,
  },
  {
    path: "/men's-haircut",
    element: <MensHaircut />,
  },
];

export const privateRoutes: Array<PathRouteProps> = [
  {
    path: "/admin",
    element: <Admin />,
  },
];
