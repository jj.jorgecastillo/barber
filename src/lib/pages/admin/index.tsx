import { Grid } from "@chakra-ui/react";

import SomeText from "./components/SomeText";

const Admin = () => {
  return (
    <Grid gap={4}>
      <SomeText />
    </Grid>
  );
};

export default Admin;
