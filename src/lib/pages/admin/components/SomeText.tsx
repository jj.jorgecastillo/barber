import { Grid, Heading, Text } from "@chakra-ui/react";

const SomeText = () => {
  return (
    <Grid textAlign="center" gap={2}>
      <Heading fontSize="2xl" fontWeight="extrabold">
        Esto es para administradores
      </Heading>
      <Text color="gray.500" fontSize="sm">
        Solo el admin ve esta parte.
      </Text>
    </Grid>
  );
};

export default SomeText;
