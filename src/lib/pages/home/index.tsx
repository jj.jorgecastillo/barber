import { Box, Center, Text } from "@chakra-ui/react";

import BarberAnimation from "./animation";

const Home = () => (
  <Center color="white">
    <Box
      h={window.innerWidth > 768 ? "auto" : "100%"}
      w={window.innerWidth > 768 ? "auto" : "100%"}
      borderRadius="lg"
      bgGradient={[
        "linear(to-tr, teal.300, yellow.400)",
        "linear(to-t, blue.200, teal.500)",
        "linear(to-b, orange.100, purple.300)",
      ]}
    >
      <Text
        bgGradient="linear(to-l, #7928CA, #FF0080)"
        bgClip="text"
        fontSize={window.innerWidth > 768 ? ["4xl", "5xl"] : ["2xl", "3xl"]}
        fontWeight="extrabold"
        align="center"
      >
        Tu barbero favorito
      </Text>
      <BarberAnimation />
      <Text
        bgGradient="linear(to-l, #7928CA, #FF0080)"
        bgClip="text"
        fontSize={window.innerWidth > 768 ? ["4xl", "5xl"] : ["2xl", "3xl"]}
        fontWeight="extrabold"
        align="center"
      >
        Corte para damas y caballeros
      </Text>
    </Box>
  </Center>
);

export default Home;
