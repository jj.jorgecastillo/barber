import Lottie from "react-lottie";

import data from "./animationData/barber.json";

const options = { loop: true, autoplay: true, animationData: data };

const BarberAnimation = () => (
  <Lottie
    options={options}
    height={window.innerWidth > 768 ? "auto" : "100%"}
    width={window.innerWidth > 768 ? "auto" : "100%"}
  />
);

export default BarberAnimation;
