import bob from "../img/bob.jpg";
import capas from "../img/capas.jpg";
import flequillo from "../img/flequillo.jpg";
import largo from "../img/largo.png";
import pixie from "../img/pixie.jpg";
import rapado from "../img/rapado.jpg";

export const CortesDamas = [
  {
    id: 1,
    price: 100,
    title: "Corte Bob",
    description:
      "Corte que llega hasta la mandíbula, con flequillo recto o desfilado.",
    urlImage: bob,
  },
  {
    id: 2,
    price: 100,
    title: "Corte Pixie",
    description:
      "Corte muy corto, que llega justo debajo de las orejas, con flequillo corto o sin él.",
    urlImage: pixie,
  },
  {
    id: 3,
    price: 100,
    title: "Corte de capas",
    description: "Corte con capas que agrega volumen y movimiento al cabello.",
    urlImage: capas,
  },
  {
    id: 4,
    price: 100,
    title: "Corte de cabello largo",
    description:
      "Corte con longitud que cae por debajo de los hombros, con flequillo largo o sin él.",
    urlImage: largo,
  },
  {
    id: 5,
    price: 100,
    title: "Corte de cabello raspado",
    description: "Corte con un lado o parte del cabello raspado o afeitado.",
    urlImage: rapado,
  },
  {
    id: 6,
    price: 100,
    title: "Corte de cabello con flequillo",
    description:
      "Corte con un flequillo largo o corto que puede ser liso o desfilado.",
    urlImage: flequillo,
  },
];
