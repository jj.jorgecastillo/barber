import clasico from "../img/clasico.jpg";
import fade from "../img/fade.jpeg";
import frances from "../img/frances.jpg";
import ivy from "../img/Ivy.png";
import militar from "../img/militar.jpg";
import rapado from "../img/rapado.jpg";

export const CortesCaballeros = [
  {
    id: 1,
    price: 50,
    title: "Corte de cabello militar",
    description:
      "corto en los lados y parte superior, con una línea precisa en la parte superior para dar un aspecto ordenado y aseado.",
    urlImage: militar,
  },
  {
    id: 2,
    price: 50,
    title: "Corte de cabello estilo Ivy League",
    description:
      "similar al corte militar, pero con un poco más de longitud en la parte superior y un flequillo largo.",
    urlImage: ivy,
  },
  {
    id: 3,
    price: 50,
    title: "Corte de cabello de estilo francés",
    description:
      "conocido por tener un estilo desordenado y voluminoso en la parte superior y corto en los laterales.",
    urlImage: frances,
  },
  {
    id: 4,
    price: 50,
    title: "Corte de cabello de estilo clásico",
    description:
      "es un corte tradicional, con una línea precisa en la parte superior y una graduación suave en los lados.",
    urlImage: clasico,
  },
  {
    id: 5,
    price: 50,
    title: "Corte de cabello de estilo desvanecido",
    description:
      "conocido por tener una transición gradual desde el cabello corto en los lados hasta el cabello más largo en la parte superior.",
    urlImage: fade,
  },
  {
    id: 6,
    price: 50,
    title: "Corte de cabello de estilo de rasurado",
    description:
      "es un corte muy corto, con el cabello rasurado o afeitado en los lados y un poco más largo en la parte superior.",
    urlImage: rapado,
  },
];
