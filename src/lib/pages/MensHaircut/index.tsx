import { SimpleGrid } from "@chakra-ui/react";
import type { FunctionComponent } from "react";

import BarberCard from "lib/components/BarberCard";

import { CortesCaballeros } from "./data/cortescaballeros";

const LadiesHaircut: FunctionComponent = () => {
  return (
    <SimpleGrid
      spacing={4}
      templateColumns="repeat(auto-fill, minmax(200px, 1fr))"
    >
      {CortesCaballeros.map(
        (corte: {
          id: number;
          title?: string;
          price?: number;
          description?: string;
          urlImage?: string;
        }) => (
          <BarberCard key={corte.id} data={corte} />
        )
      )}
    </SimpleGrid>
  );
};

export default LadiesHaircut;
