import {
  Card,
  CardBody,
  CardFooter,
  Image,
  Text,
  Heading,
  Stack,
  ScaleFade,
  Accordion,
  AccordionItem,
  AccordionButton,
  Box,
  AccordionPanel,
  Badge,
} from "@chakra-ui/react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";

import corte1 from "../../../../../assets/img/mens/fade.jpeg";

export const BarberCard = () => {
  return (
    <Card maxW="sm">
      <CardBody>
        <ScaleFade initialScale={0.9} in>
          <Image src={corte1} alt="Corte Fade con rayas" borderRadius="lg" />
        </ScaleFade>
        <Stack mt="2" spacing="2">
          <Heading size="xs">Fade con rayas</Heading>
          <Badge borderRadius="full" px="8" m="auto" colorScheme="teal">
            <Text color="green.600" fontSize="xl" align="center">
              $20
            </Text>
          </Badge>
        </Stack>
      </CardBody>
      <CardFooter>
        <Accordion allowMultiple>
          <AccordionItem>
            {({ isExpanded }) => (
              <>
                <AccordionButton
                  _expanded={{ bg: "green.300", color: "white" }}
                >
                  <Box as="span" flex="1" textAlign="left">
                    Descripción del corte
                  </Box>
                  {isExpanded ? (
                    <AiFillEye fontSize="18px" />
                  ) : (
                    <AiFillEyeInvisible fontSize="18px" />
                  )}
                </AccordionButton>
                <AccordionPanel>
                  <Text>
                    El corte <strong>Fade con rayas</strong> es un estilo
                    popular entre los hombres que combina un corte de fade con
                    un toque de estilo. El fade se refiere a un corte de cabello
                    en el que la longitud va disminuyendo gradualmente desde la
                    coronilla hasta las patillas y la nuca. En este estilo, se
                    añaden rayas en la cabeza, que pueden ser de varios tamaños
                    y direcciones, para agregar un toque de personalidad y
                    originalidad. Es un corte versátil que se adapta a una
                    variedad de estilos y personalidades y es fácil de mantener
                    con una simple visita regular al barbero
                  </Text>
                </AccordionPanel>
              </>
            )}
          </AccordionItem>
        </Accordion>
      </CardFooter>
    </Card>
  );
};
