import {
  Box,
  Circle,
  Flex,
  Heading,
  HStack,
  Spacer,
  Text,
  Tooltip,
} from "@chakra-ui/react";
import { FcBusinessman, FcBusinesswoman } from "react-icons/fc";
import { Link } from "react-router-dom";

import IconBarber from "./IconBarber";
import ThemeToggle from "./ThemeToggle";

const Header = () => {
  return (
    <Flex
      as="header"
      width="full"
      align="center"
      alignSelf="flex-start"
      justifyContent="center"
      gridGap={2}
    >
      <Box p="2">
        <HStack>
          <Link to="/">
            <Circle size="40px" bg="blue.200" color="white">
              <IconBarber />
            </Circle>
          </Link>
          <Heading size="md">
            <Text bgGradient="linear(to-l, #285eca, #4dff00)" bgClip="text">
              Barber Style
            </Text>
          </Heading>
        </HStack>
      </Box>
      <Spacer />
      <HStack>
        <Tooltip hasArrow label="Corte de Damas" bg="pink.500">
          <Link to="ladies-haircut">
            <Circle size="40px" bg="purple.200" color="white">
              <FcBusinesswoman fontSize={30} />
            </Circle>
          </Link>
        </Tooltip>
        <Tooltip hasArrow label="Corte de Caballeros" bg="green.500">
          <Link to="men's-haircut">
            <Circle size="40px" bg="green.200" color="white">
              <FcBusinessman fontSize={30} />
            </Circle>
          </Link>
        </Tooltip>
      </HStack>
      <Box marginLeft="auto">
        <ThemeToggle />
      </Box>
    </Flex>
  );
};

export default Header;
