import {
  Card,
  CardBody,
  CardFooter,
  Image,
  Text,
  Heading,
  Stack,
  ScaleFade,
  Accordion,
  AccordionItem,
  AccordionButton,
  Box,
  AccordionPanel,
  Badge,
} from "@chakra-ui/react";
import type { FunctionComponent } from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";

type BarberCardProps = {
  data: {
    id: number;
    title?: string;
    price?: number;
    description?: string;
    urlImage?: string;
  };
};

const BarberCard: FunctionComponent<BarberCardProps> = ({
  data,
}: BarberCardProps) => {
  const { title, price, description, urlImage } = data;

  return (
    <Card maxW="sm">
      <CardBody>
        <ScaleFade initialScale={0.9} in>
          <Image w={314} h={200} src={urlImage} alt={title} borderRadius="lg" />
        </ScaleFade>
        <Stack mt="2" spacing="2">
          <Heading size="xs">{title}</Heading>
          <Badge borderRadius="full" px="8" m="auto" colorScheme="teal">
            <Text color="green.600" fontSize="xl" align="center">
              ${price}
            </Text>
          </Badge>
        </Stack>
      </CardBody>
      <CardFooter>
        <Accordion allowMultiple>
          <AccordionItem>
            {({ isExpanded }) => (
              <>
                <AccordionButton
                  _expanded={{ bg: "green.300", color: "white" }}
                >
                  <Box as="span" flex="1" textAlign="left">
                    Descripción del corte
                  </Box>
                  {isExpanded ? (
                    <AiFillEye fontSize="18px" />
                  ) : (
                    <AiFillEyeInvisible fontSize="18px" />
                  )}
                </AccordionButton>
                <AccordionPanel>
                  <Text>{description}</Text>
                </AccordionPanel>
              </>
            )}
          </AccordionItem>
        </Accordion>
      </CardFooter>
    </Card>
  );
};
export default BarberCard;
